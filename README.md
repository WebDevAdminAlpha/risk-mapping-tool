![](assets/banner.png)

The Risk Mapping Tool was built with [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), [Jekyll](https://jekyllrb.com/) and [Boostrap](https://getbootstrap.com/). Uses [Issues API](https://docs.gitlab.com/ee/api/issues.html) and [Links API](https://docs.gitlab.com/ee/api/issue_links.html) to fetch data.

## 🛠 Configuration

### Labels

Ensure the project has the labels needed to manage risks. The tool depends on certain labels to process its data correctly.

- `~"risk"` for risks that get to be pulled into the map. The issue tracker may contain other kinds of issues that are not risks such as risk management tasks
- `~"risk::group-map"` for Group Risks which do not fall under a specific product category
- `~"Category::<Category>"` are applied on risk issues to categorise them and create Category Risk Maps
- Add risk impact and risk probability labels to assess the overall Risk Score (priority)

| Impact           | Probability           |
| ---------------- | --------------------- |
| ~"risk-impact::1" | ~"risk-probability::1" |
| ~"risk-impact::2" | ~"risk-probability::2" |
| ~"risk-impact::3" | ~"risk-probability::3" |
| ~"risk-impact::4" | ~"risk-probability::4" |
| ~"risk-impact::5" | ~"risk-probability::5" |

**Note:** Only the `~"Category::<Category>"` exists by default in a project within the GitLab group. The other mentioned labels need to be created in the Risk Project by going to **Issues** > **Labels**. 
 ### 🗺 Maps

Two sets of maps can be created: 
 * Group Maps
 * Category Maps

A Group Map is Risk Management done at the [Group level](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/stages.yml). These can include common Risk Areas such as Team, Infrastructure, Quality, User Experience risks that are applicable to the Group in a broader sense. 

A Category Map is done at the [Category level](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/categories.yml) representing a standalone product that has [Features](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml). These maps should be more technical and focused on the particularities of the product. 

* `index.html` defines the Group Map
* `categories` folder can include multiple `category_name.html` for Category Maps

#### Category Map

Use the `rocket_fuel.html` as a Category Map template by renaming it to the desired `category_name.html`.

Replace

```yml
---
layout: default
title: Rocker Fuel # Category Name
---
{% assign category_label = 'Category:Rocket Fuel' %} # Category:Label to fecth the Category risks
```

#### _data/vars.yml

```yml
group_map_path: "/" # the general group map is at the root of the site
group: Explorers Group # the group name
handbook: https://about.gitlab.com/handbook/engineering/development/<team_page> #handbook page for the group
risk_project: https://gitlab.com/gitlab-org/<project base_url> # base_url is the /subgroups/project path of the risk project
categories: # for each map add an entry
  - name: Spaceship Cabin
    file_path: "categories/space_cabin.html" # category maps are stored in the categories folder
  - name: Rocket Fuel
    file_path: "categories/rocket_fuel.html"
pipeline_new: "https://gitlab.com/<project base_url>/-/pipelines/new" #path to the new pipeline that maps with the manual refresh button
```

### Populate

#### Risks

The Risk Project Issue Tracker documents the Risk Issues, created with a Risk template.

The template guides through the considerations needed when adding a new Risk such as its Description, Impact and Possible Mitigation.
The labels need to be selected according to where the Risk belongs in the Map and a Risk Score is calculated by multiplying Impact Level with Probability. The Risk Score is represented in the Risk issue as it's Weight value.

The Risk Impact and Risk Probability labels can be adjusted as the Risk gets mitigated. The Risk Score should also be adjusted accordingly.

#### Mitigations

Issues that reduce the severity of a risk's impact or the probability of its occurence are known as Mitigations.

Work that contributes to mitigate a risk should be associated to the Risk Issue as a "relates to" relationship, forming a link between these two issues. This surfaces the Mitigations under a given Risk in the Risk Map by showing when they are scheduled and what is their current state. This is key for increasing Transparency, Visibility and Effectiveness.     

---
### Run locally

1. Clone this project
2. Run `bundle exec ruby fetcher.rb -p <risk_project_id>` to fetch issues from a specified risk project
3. Run `jekyll serve --livereload` to serve the Page
4. Open `http://127.0.0.1:4000/<base_url>/`

**Note:** This is assuming Jekyll is installed on the machine. See [Jekyll installation instructions](https://jekyllrb.com/docs/installation/).

### Continuous Risks Integration

Now that all necessary data is prepared it's time to get the Risk Map live 🚀 On the Project page, first configure an `ACCESS_TOKEN` CI Variable with permission to read the issues of the project in **Settings** > **CI/CD** > **Variables**. 

Once that is done, go to **CI/CD** > **Pipelines** and "Run Pipeline". It should take a few moments to run the job. To find what is the URL where the map is deployed at go to **Settings** > **Pages** and open the URL in "Your pages are served under". 

⚠️ Currently, refreshing the data pulled into the Risk Map is a manual process. When adding a Risk to the Project Tracker, or linking a Risk and a Mitigation issue, the data needs to be synced by either running a new pipeline or locally running `bundle exec ruby fetcher.rb -p <risk_project_id>`. Consider adding a pipeline schedule to sync the Map a few times a day. 

---
### Project Structure

```
.
+-- _config.yml ---> adjust Base URL to serve the website within a GitLab Group
+-- _data
|   +-- json/ ---> json database for issues and their links
|   +-- vars.yml ---> configuration values to adjust when personalising the Risk Map
+-- _includes
+-- _layouts
+-- _categories ---> stores category maps
|   +-- rocket_fuel.html ---> a category map
|   +-- ...
+-- assets ---> images, icons
+-- _site
+-- index.html ---> group risk map, main page
+-- fetcher.rb ---> simple ruby script to fetch data to _data folder
```
