#!/usr/bin/ruby

require 'httparty'
require 'optparse'
require 'fileutils'

hash_options = {}
OptionParser.new do |opts|
  opts.on('-p [project_id]', '--project-id [project_id]', "Specify the project_id or use $CI_PROJECT_ID") do |v|
    hash_options[:project_id] = v
  end
end.parse!

project_id =  hash_options[:project_id]
url_issues = "https://gitlab.com/api/v4/projects/#{project_id}/issues"
issues_filter = '?state=opened&labels=risk&per_page=100'
 
options = {
  :headers => {  
    "Authorization" => "Bearer #{ENV['ACCESS_TOKEN']}",
    'Content-Type' => 'application/json',
    'Accept' => 'application/json'
  }
}

response = HTTParty.get(url_issues + issues_filter, options)

FileUtils.mkdir_p '_data/json'

File.open('_data/json/issues.json', 'w') do |file|
  file.write(response)
end

issues_iids = response.map { |issue| issue['iid'] }

issues_iids.each do |iid|
  issue_links = HTTParty.get(url_issues + "/#{iid}/links", options)
    File.open("_data/json/#{iid}.json", 'w') do |file|
      file.write issue_links
    end
end